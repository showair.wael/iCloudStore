//
//  iCloud+CollectionTestCase.swift
//  ExprimentalAppTests
//
//  Created by innovapost on 2017-09-21.
//  Copyright © 2017 Swifter. All rights reserved.
//

//
//  ExprimentalAppTests.swift
//  ExprimentalAppTests
//
//  Created by innovapost on 2017-09-11.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest
import CloudKit
import iCloudStore
import RxSwift

@testable import ExprimentalApp

extension XCTestCase {
  var defaultTimeout: TimeInterval { return TimeInterval(5) }
}

class ICloudTestCase: XCTestCase {
  let iCloud = ICloudStore()
  let bag = DisposeBag()
  let iCloudZone = CKRecordZone(zoneName: Person.iCloudZoneName) // the zone’s owner is set to the current user automatically
  let iCloudSubscription =
    CKRecordZoneSubscription(zoneID: CKRecordZoneID(zoneName:Person.iCloudZoneName,ownerName:CKRecordZoneDefaultName ),
                             subscriptionID: "Person Subscription")
  
  let persons = [
    Person(name: "name-1", age: 13, uuid: UUID().uuidString),
    Person(name: "name-2", age: 34, uuid: UUID().uuidString),
    Person(name: "name-3", age: 25, uuid: UUID().uuidString),
    Person(name: "name-4", age: 10, uuid: UUID().uuidString),
    ]
  
  override func setUp() {
    super.setUp()
    let expectation = XCTestExpectation(description: "ICloudTestCase: Setup is completed")
    //TODO:- Add atomic to the operation configuration object or as an extra parameter to the rx.push method
    //    let operation = CKModifyRecordsOperation()
    //    operation.isAtomic = false
    // Create a custom zone, a zone subscription then  push 4 records into that custom zone
    self.iCloudZone.rx.create()
      .flatMap { (_) -> Observable<CKSubscription> in
        self.iCloudSubscription.notificationInfo = CKNotificationInfo()
        return self.iCloudSubscription.rx.create()
      }
      .flatMap { (_) -> Observable<CKRecord> in
        return self.iCloud.rx.push(self.persons)
      }
      .subscribe(
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted: { expectation.fulfill()}
      )
      .disposed(by: bag)
    wait(for: [expectation], timeout: defaultTimeout)
  }
  
  override func tearDown() {
    let expectation = XCTestExpectation(description: "ICloudTestCase: TearDown is completed")
    //MARK:- Deleting a zone, will consequently, delete any zone records/subscriptions
    self.iCloudZone.rx.delete()
      .subscribe(
        onError: { XCTAssert(false, "********** \(expectation) **********\n\($0)") },
        onCompleted:{expectation.fulfill()}
      )
      .disposed(by: bag)
    
    wait(for: [expectation], timeout: defaultTimeout)
    super.tearDown()
  }
}

