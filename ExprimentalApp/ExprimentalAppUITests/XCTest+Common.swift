//
//  XCTest+Common.swift
//  ExprimentalAppUITests
//
//  Created by innovapost on 2017-11-05.
//  Copyright © 2017 Swifter. All rights reserved.
//

import XCTest

extension XCTest{
  func XCTestAssertExistence(of element:XCUIElement, within timeout:TimeInterval=TimeInterval(2))  {
    let expectation =
      XCTNSPredicateExpectation(predicate: NSPredicate(format: "true == exists"),
                                object: element)
    let result = XCTWaiter.wait(for:[expectation], timeout: timeout)
    XCTAssert(result == .completed)
    
  }
}
