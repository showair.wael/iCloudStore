//
//  ViewController+AccountErrors.swift
//  ExprimentalApp
//
//  Created by innovapost on 2017-11-12.
//  Copyright © 2017 Swifter. All rights reserved.
//

import UIKit

extension ViewController{
  func onNonAuthenticated (){
    DispatchQueue.main.async {
      self.operationStatus.text = "Not Authenticated"
      self.operationStatus.textColor = UIColor.red
    }
  }
}
