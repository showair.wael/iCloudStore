//
//  ViewController+NetworkErrors.swift
//  ExprimentalApp
//
//  Created by innovapost on 2017-11-03.
//  Copyright © 2017 Swifter. All rights reserved.
//
import UIKit
import CloudKit
import RxSwift
import iCloudStore

extension ViewController{
  
  func onNetworkUnavailable (){
    DispatchQueue.main.async {
      self.operationStatus.text = "Network Unavailable"
      self.operationStatus.textColor = UIColor.red
    }
  }
  
  func onNetworkFailure(){
    DispatchQueue.main.async {
      self.operationStatus.text = "Network Failure"
      self.operationStatus.textColor = UIColor.red
    }
  }
  
  @IBAction func triggerNetworkUnavailable(_ sender: UIButton) {
    let configuration = CKOperationConfiguration()
    configuration.qualityOfService = .userInitiated
    self.push(usingConfiguration: configuration)
  }
  
  @IBAction func triggerNetworkFailure(_ sender: UIButton) {
    let configuration = CKOperationConfiguration()
    configuration.timeoutIntervalForResource = 0.2
    configuration.timeoutIntervalForRequest = 0.2
    self.push(usingConfiguration: configuration)
  }
  
}
