//
//  iCloud+Subscription.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-26.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

extension Reactive where Base == ICloudStore {
  public func fetchAllSubscriptions()->Observable<[CKSubscription]>{
    return Observable.create({ (observer) -> Disposable in
      let operation = CKFetchSubscriptionsOperation.fetchAllSubscriptionsOperation()
      operation.fetchSubscriptionCompletionBlock = { dictionary, error in
        if let error = error { observer.onError(error) }
        else if let dictionary = dictionary { observer.onNext(Array<CKSubscription>(dictionary.values))}
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
}
