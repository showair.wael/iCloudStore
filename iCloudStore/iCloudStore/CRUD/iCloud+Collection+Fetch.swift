//
//  iCloud+Collection+Fetch.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-22.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

//TODO:- Customization Points:
/*
 * 1. desired keys to fetch
 * 2. operation quality of service
 * 3. Operation Group and Operation Group Configuration
 * 4. Does the observer interesed in receiving full record or partial records?
 * 5. In what operation queue can we execute the operation or execute it manually?
 
 The first three points are operation configuration, the fourth can be ignored because the operator emits CKRecord whether it is partial or not, Lets leave that to the consumer.
 For executing an operation, I choose to implement my own operation queue, just to have full control over every thing.
 */
extension Reactive where Base == ICloudStore {
  public func fetch(_ fetchables:[ICloudStorable], usingOperationConfiguration configuration:CKOperationConfiguration?=nil)->Observable<CKRecord>{
    return Observable.create({ (observer) -> Disposable in
      let operation = CKFetchRecordsOperation()
      if let configuration = configuration{
        operation.configuration = configuration
      }
      operation.recordIDs = fetchables.map{$0.toCloudObject().recordID}
      operation.perRecordCompletionBlock = { record, _ , _ in
        if let record = record { observer.onNext(record) }
      }
      
      operation.fetchRecordsCompletionBlock = { _ , error in
        if let error = error { observer.onError(error) }
        observer.onCompleted()
      }
      
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
}
