//
//  iCloud+Collection+Push.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-21.
//  Copyright © 2017 Swifter. All rights reserved.
//

import RxSwift
import CloudKit

//TODO:- Customization Points:
/*
 * 1. is Atomic operation
 * 2. operation quality of service
 * 3. Operation Group and Operation Group Configuration
 * 4. Save Policy for merge conflicts
 * 5. Does the observer interesed in receiving saved records one-by-one or one-shot?
 * 6. In what operation queue can we execute the operation or execute it manually?
 * 7. client changes token: check answers here: https://goo.gl/P5kM8V
 
 The first four points are operation configuration, the fifth one is dependent whether the opreation is atomic or not.
 For executing an operation, I choose to implement my own operation queue, just to have full control over every thing.
 */
extension Reactive where Base == ICloudStore {
  public func push(storables:[ICloudStorable]?=nil,
                   andDelete deletables:[ICloudStorable]?=nil,
                   usingOperationConfiguration configuration:CKOperationConfiguration?=nil )->Observable<ICloudPushResult>{
    return Observable.create({ (observer) -> Disposable in
      
      let records = storables?.map{$0.toCloudObject()}
      let recordsIDs = deletables?.map{ $0.toCloudObject().recordID }
      
      let operation = CKModifyRecordsOperation()
      if let configuration = configuration{
        operation.configuration = configuration
      }
      operation.recordsToSave = records
      operation.recordIDsToDelete = recordsIDs
      
      if !operation.isAtomic{
        operation.perRecordCompletionBlock = { record, error in
          observer.onNext(ICloudPushResult.singleRecordResultType(record))
        }
      }
      operation.modifyRecordsCompletionBlock = { records , recordsIDs , error in
        if let error = error { observer.onError(error); return }
        if let recordsIDs = recordsIDs, recordsIDs.count>0 {
          observer.onNext(ICloudPushResult.allRecordsIDsResultType(recordsIDs))
        }
        if let records = records, records.count>0, operation.isAtomic {
          observer.onNext(ICloudPushResult.allRecordsResultType(records))
        }
        observer.onCompleted()
      }
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
      
    })
  }
}


