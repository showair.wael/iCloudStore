//
//  Account.swift
//  iCloudStore
//
//  Created by innovapost on 2017-11-07.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

#if DEBUG
  extension CKAccountStatus: CustomStringConvertible{
    public var description: String {
      switch self {
      case .couldNotDetermine:
        return "Couldn't Determine"
      case .noAccount:
        return "No Account"
      case .available:
        return "Available"
      case .restricted:
        return "Restricted"
      }
    }
  }
#endif

extension ICloudStore{
  public var account: ICloudAccount{
    return ICloudAccount.shared
  }
  
  public class ICloudAccount {
    public static let shared = ICloudAccount()
    public private(set) var status: Variable<CKAccountStatus>
    private init () {
      status = Variable(CKAccountStatus.available)
      //The notification is sent by an instance of the CKContainer class. If there are no instances of the class, notifications are not sent.
      //Accordingly, I have to have a strong reference to default container which I did in ICloudStore object ;)
      NotificationCenter.default.addObserver(self, selector: #selector(onChangeStatus), name: NSNotification.Name.CKAccountChanged, object: nil)
    }
    
    @objc func onChangeStatus() {
      CKContainer.default().accountStatus { (iCloudStatus, error ) in
        if let _ = error{
          self.status.value = .couldNotDetermine
        }else{
          self.status.value = iCloudStatus
        }
      }
    }
    
    deinit {
      NotificationCenter.default.removeObserver(self)
    }
  }
}
