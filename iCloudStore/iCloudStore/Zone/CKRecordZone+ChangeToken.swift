//
//  CKRecordZone+ServerChangeToken.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-28.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit

extension CKRecordZone {
  var ZONE_SERVER_CHANGE_TOKEN_KEY:String {return "zoneServerChangeToken" }
  public var changeToken: CKServerChangeToken? {
    get {
      let storedToken = UserDefaults.standard.object(forKey:ZONE_SERVER_CHANGE_TOKEN_KEY) as? Data
      guard let archivedToken = storedToken else { return nil }
      //TODO:- Use CKServerChangeToken(coder:) instead of manually unacrhving the object. Watch 2017 video: what is new in Foundation
      return NSKeyedUnarchiver.unarchiveObject(with: archivedToken) as! CKServerChangeToken?
    }
    set(token){
      let userDefaults = UserDefaults.standard
      let archivedToken:Data? = (token != nil) ? NSKeyedArchiver.archivedData(withRootObject: token!) : nil
      userDefaults.set(archivedToken, forKey: ZONE_SERVER_CHANGE_TOKEN_KEY)
      userDefaults.synchronize()
    }
  }
}
