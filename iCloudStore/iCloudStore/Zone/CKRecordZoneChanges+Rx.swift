//
//  iCloud+ZoneChanges.swift
//  iCloudStore
//
//  Created by innovapost on 2017-09-28.
//  Copyright © 2017 Swifter. All rights reserved.
//

import CloudKit
import RxSwift

extension Reactive where Base: CKRecordZone {
  public func fetchChanges(since token :CKServerChangeToken?=nil, usingOperation operation: CKFetchRecordZoneChangesOperation = CKFetchRecordZoneChangesOperation(), withOptions options:CKFetchRecordZoneChangesOptions=CKFetchRecordZoneChangesOptions()) ->Observable<ICloudFetchResult>{
    return Observable.create({ (observer) -> Disposable in
      operation.recordZoneIDs = [self.base.zoneID]
      options.previousServerChangeToken = token
      operation.optionsByRecordZoneID = [self.base.zoneID:options]
      
      operation.recordChangedBlock = { record in
        observer.onNext(ICloudFetchResult.singleRecordResultType(record))
      }
      
      operation.recordWithIDWasDeletedBlock = { recordID, recordType in
        observer.onNext(ICloudFetchResult.compositeRecordID(recordID, recordType))
      }
      
      //TODO:- Make use of clientChangeTokenData
      operation.recordZoneChangeTokensUpdatedBlock = { zoneID, newZoneChangeToken, _ in
        let zone = CKRecordZone(zoneID: zoneID)
        zone.changeToken = newZoneChangeToken
      }
      
      operation.fetchRecordZoneChangesCompletionBlock = {error in
        if let error = error {observer.onError(error)}
      }
      
      operation.recordZoneFetchCompletionBlock = { zoneID, newZoneChangeToken, _, _, error in
        let zone = CKRecordZone(zoneID: zoneID)
        zone.changeToken = newZoneChangeToken
        if let error = error {observer.onError(error)}
        else {
          observer.onCompleted()
        }
      }
      
      CKContainer.default().privateCloudDatabase.add(operation)
      return Disposables.create()
    })
  }
}
